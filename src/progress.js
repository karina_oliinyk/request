document.getElementById('uploadForm').onsubmit = function(e) {
  e.preventDefault();
  const form = new FormData();
  form.append('sampleFile', e.target.sampleFile.files[0]);
  fetch('http://localhost:8000/upload', {
    method: 'POST',
    body: form
  });
};